#  Color-Tiles
#  Copyright (C) 2019  Cameron Himes

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from random import randint as rint
import pygame
from time import sleep
from pygame.locals import *

#settings
tile_size = 15
darts_per_second = 8
screen_height = 500
screen_width = 1000
is_automatic = True

#vars for later
dart_delay = 1 / darts_per_second

#initalize pygame
print("[EVENT] Starting Pygame...")
pygame.init()
screen=pygame.display.set_mode((screen_width,screen_height),HWSURFACE|DOUBLEBUF|RESIZABLE)
pygame.display.set_caption("Color Tiles v.0.1")
pygame.display.flip()

#make system to color a pixel
def drawTile(start_x,start_y,end_x,end_y):
    use_color = (rint(0,255),rint(0,255),rint(0,255)) # make color random
    for use_y in range(start_y,end_y):
        for use_x in range(start_x,end_x):
            use_pos = [use_x,use_y]
            pygame.draw.line(screen,use_color,use_pos,use_pos,1)

def throwDart():
    # throw dart
    dart_pos = [rint(0,screen_width),rint(0,screen_height)] # [x,y]
    # get tile positions
    use_tile = [
        dart_pos[0] - (dart_pos[0] % tile_size),
        dart_pos[1] - (dart_pos[1] % tile_size),
        dart_pos[0] + (tile_size - (dart_pos[0] % tile_size)),
        dart_pos[1] + (tile_size - (dart_pos[1] % tile_size))
    ]
    # tile positions are not uniform at boarder, fix positions if necessary
    # check X
    if dart_pos[0] > (screen_width - (screen_width % tile_size)):
        use_tile[2] = screen_width # fix x
    # check y
    if dart_pos[1] > (screen_height - (screen_height % tile_size)):
        use_tile[3] = screen_height # fix y
    drawTile(use_tile[0],use_tile[1],use_tile[2],use_tile[3])
    pygame.display.flip()

def clearBoard():
    screen.fill((255,255,255))
    pygame.display.flip()

def exit_game():
    print("[EVENT] Exiting program...")
    pygame.display.quit()
    pygame.quit()
    exit()

#load initial window
print("[INFO] Drawing initial window...")
clearBoard()

while True:
    pygame.event.pump()
    event=pygame.event.poll()
    if event.type==QUIT:
        exit_game()
    elif event.type==VIDEORESIZE:
        screen=pygame.display.set_mode(event.dict['size'],HWSURFACE|DOUBLEBUF|RESIZABLE)
        print("[EVENT] Screen resizing to %s x %s..." % (event.dict['size'][0],event.dict['size'][1]))
        screen_width = event.dict['size'][0]
        screen_height = event.dict['size'][1]
        clearBoard()
        print("[EVENT] Resize Complete.")
    elif event.type==KEYDOWN:
        print("[EVENT] Keypress detected for '%s'." % (event.dict['unicode']))

        # throw dart
        if (event.dict['unicode'] == "d") or (event.dict['unicode'] == "t"):
            throwDart()
            print("[INFO] Dart thrown manually.")
        
        # toggle automatic
        elif (event.dict['unicode'] == "a") or (event.dict['unicode'] == "m"):
            is_automatic = not is_automatic # toggle state
            if is_automatic:
                print("[INFO] Drawing mode is now automatic.")
            else:
                print("[INFO] Drawing mode is now manual.")
        
        # increase draw speed
        elif (event.dict['unicode'] == "+") or (event.dict['unicode'] == "w") or (event.dict['unicode'] == "="):
            darts_per_second += 1 # increase speed
            dart_delay = 1 / darts_per_second # update delay variable
            print("[INFO] Drawing rate is now %s tiles per second." % (darts_per_second))
        
        # decrease draw speed
        elif (event.dict['unicode'] == "-") or (event.dict['unicode'] == "s"):
            darts_per_second -= 1 # decrease speed
            dart_delay = 1 / darts_per_second # update delay variable
            print("[INFO] Drawing rate is now %s tiles per second." % (darts_per_second))
        
        # clear board
        elif (event.dict['unicode'] == "e") or (event.dict['unicode'] == "c"):
            clearBoard()
            print("[INFO] Board cleared.")
        
        # exit
        elif (event.dict['unicode'] == "\x1b") or (event.dict['unicode'] == "q"):
            # exit message is within the function
            exit_game()
    elif event.type==NOEVENT and is_automatic:
        throwDart()
        sleep(dart_delay)

#       ~~~~~~~~~~~~~~~~~~~~~~~
#     ~~~~~ KEYBOARD LAYOUT ~~~~~
#       ~~~~~~~~~~~~~~~~~~~~~~~
#
#  _________________________________________________________________________________________________________________________
#  |           |           |           |           |           |           |           |           |           |           |                                      
#  |     Q     |     W     |     E     |     R     |     T     |     Y     |     U     |     I     |     O     |     P     |
#  |    EXIT   |  SPEED +  |   CLEAR   |           |    DART   |           |           |           |           |           |
#  |___________|___________|___________|___________|___________|___________|___________|___________|___________|___________|
#        |           |           |           |           |           |           |           |           |           |                                      
#        |     A     |     S     |     D     |     F     |     G     |     H     |     J     |     K     |     L     |
#        |    AUTO   |  SPEED -  |    DART   |           |           |           |           |           |           |
#        |___________|___________|___________|___________|___________|___________|___________|___________|___________|
#              |           |           |           |           |           |           |           |                                      
#              |     Z     |     X     |     C     |     V     |     B     |     N     |     M     |
#              |           |           |   CLEAR   |           |           |           |    AUTO   |
#              |___________|___________|___________|___________|___________|___________|___________|
